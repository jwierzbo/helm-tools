.PHONY: run
run: fmt
	go run -mod=vendor ./...

.PHONY: build
build: fmt
	go build -mod=vendor

.PHONY: fmt
fmt:
	go fmt

.PHONY: update
update:
	go mod vendor -v
