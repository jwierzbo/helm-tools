/*
Copyright (C) 2018 jwierzbo

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"log"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	rspb "k8s.io/helm/pkg/proto/hapi/release"
)

const helmTillerNamespace = "edu"
const helmChartRevisionConfigMap = "edu-blue-dev02.v44"

func main() {
	_, client := GetK8sConfigAndClient()

	cmHelm, _ := client.CoreV1().ConfigMaps(helmTillerNamespace).Get(helmChartRevisionConfigMap, metav1.GetOptions{})
	helmData := cmHelm.Data["release"]

	ds, err := decodeRelease(helmData)
	if err != nil {
		panic(err)
	}

	ds.Info.Status.Code = rspb.Status_DEPLOYED
	ds.Info.Description = "Install complete"

	output, err := encodeRelease(ds)
	if err != nil {
		panic(err)
	}
	cmHelm.Data["release"] = output
	cmHelm.ObjectMeta.Labels["STATUS"] = "DEPLOYED"

	// TODO uncomment for auto update
	/*_, err := client.CoreV1().ConfigMaps(helmTillerNamespace).Update(cmHelm)
	if err != nil {
		panic(err)
	}*/

	// use this string for manual update
	log.Println(output)
}
