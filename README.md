# helm-tools

Small tool for overwrite helm revision history from `FAILED` to `DEPLOYED`. Requires configured `kubectl`


## Usage

Golang 1.11 is required.

Set proper value for `helmTillerNamespace` and `helmChartRevisionConfigMap` in `main.go`, then run:

```bash
make run
```
